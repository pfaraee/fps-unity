﻿#pragma strict

var health = 100;
var animationClips : AnimationClip[];
var animationClip : AnimationClip;
var enemyAI : EnemyAI;
var isDead : boolean;

function Update(){
	if (health <= 0 && !isDead){
		Dead();
	}
}

function ApplyDamage(damage : int){
	health -= damage;
}

function Dead(){
	var animations = ["back_fall", "right_fall", "left_fall"];
	var randomAnimation = animations[Random.Range(0,2)];
	gameObject.GetComponent.<Animation>().CrossFade("right_fall");
	enemyAI.isInjured = true;
	isDead = true;
	Destroy(gameObject, 1);
}