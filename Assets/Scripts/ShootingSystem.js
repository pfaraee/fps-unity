#pragma strict
import UnityEngine.UI;


var weapon : Transform;
var  effect : Transform;
var damage : float = 50.0;
var sound : AudioClip;
var maxClipsize : int = 24;
var currentClipsize : int;
var currentAmmo : int;
var reloadAmmo : int;
var isReloading : boolean;
var playerInterface : Text;
var headShot : Text;
var headShotSound : AudioClip;

var reloadSound : AudioClip;
var emptyClipSound : AudioClip;
var playerCamera : Camera;
var muzzle : Transform;

function Start () {
	currentAmmo = 8;
	currentClipsize = 10;
	playerInterface = playerInterface.GetComponent(Text);
	// Ammo
	playerInterface.text = "Ammo: " + currentClipsize.ToString() + "/" + currentAmmo.ToString();
}

function Update () {

	var hit : RaycastHit;
	// Use Screen.height because many functions (like this one) 
	//start in the bottom left of the screen, while MousePosition starts in the top left
	var ray : Ray = Camera.main.ScreenPointToRay(Vector3(Screen.width/2, 
	Screen.height/2,0));
	
	if(Input.GetKeyDown(KeyCode.R)){
		StartCoroutine(reloadGun());
	}
	
	// Shooting
	if(Input.GetButtonDown("Fire1") && currentClipsize > 0 && !isReloading){
		currentClipsize -= 1;
		playerInterface.text = "Ammo: " + currentClipsize.ToString() + "/" + currentAmmo.ToString();
	
		if (Physics.Raycast (ray, hit, 100, Physics.kDefaultRaycastLayers)) {
			if(hit.transform.tag == "Exploadable"){
				hit.transform.SendMessage("Expload", damage, SendMessageOptions.DontRequireReceiver);
			} else if (hit.collider.transform.tag == "Head"){
				Instantiate(effect, hit.point, Quaternion.LookRotation(hit.normal));
        	 	hit.transform.SendMessage("ApplyDamage", damage*3, SendMessageOptions.DontRequireReceiver);
        	 	headShot.GetComponent(Headshot).headShot();
        	 	AudioSource.PlayClipAtPoint(headShotSound, transform.position);
			} else {
         		Instantiate(effect, hit.point, Quaternion.LookRotation(hit.normal));
        	 	hit.transform.SendMessage("ApplyDamage", damage, SendMessageOptions.DontRequireReceiver);
         	}
         	muzzle.GetComponent("MuzzleFlash").SendMessage("show");
         	AudioSource.PlayClipAtPoint(sound, transform.position);
	 	}
		weapon.GetComponent.<Animation>().Play("GunShooting");
	} else if(Input.GetButtonDown("Fire1") && currentClipsize ==0){
		AudioSource.PlayClipAtPoint(emptyClipSound, transform.position);
	}
	// Idle
if (weapon.GetComponent.<Animation>().isPlaying == false){
		weapon.GetComponent.<Animation>().CrossFade("GunIdle");
	}
	// Running
	if (Input.GetKey(KeyCode.LeftShift)){
		weapon.GetComponent.<Animation>().CrossFade("GunRun");
	}
	
	if(Input.GetMouseButtonDown(1)){
		playerCamera.fieldOfView = 30;
	} else if(Input.GetMouseButtonUp(1)){
		playerCamera.fieldOfView = 60;
	}
}

function addAmmo(ammo : int){
	currentAmmo += ammo;
	playerInterface.text = "Ammo: " + currentClipsize.ToString() + "/" + currentAmmo.ToString();
	AudioSource.PlayClipAtPoint(reloadSound, transform.position);
}

function reloadGun(){
	if((currentClipsize < maxClipsize) && currentAmmo > 0){
		isReloading = true;
		playerInterface.text = "Ammo: " + currentClipsize.ToString() + "/" + currentAmmo.ToString() + " Reloading...";
		yield WaitForSeconds(2.0);
		var reloadAmmo = maxClipsize - currentAmmo;
		if(currentAmmo >= reloadAmmo){
			currentAmmo -= reloadAmmo;
			currentClipsize = 24;
		} else if(currentAmmo < reloadAmmo){
			// prevents negative ammo
			currentClipsize += currentAmmo;
			currentAmmo = 0;
		}
		isReloading = false;
		playerInterface.text = "Ammo: " + currentClipsize.ToString() + "/" + currentAmmo.ToString();
		AudioSource.PlayClipAtPoint(reloadSound, transform.position);
	}
}