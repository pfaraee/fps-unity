﻿#pragma strict

var damage : int = 50;
var distance : float;
var maxDistance : float = 1.5;
var weapon : Transform;

function Update(){
	
	if(Input.GetButtonDown("Fire1")){
		
		//Attack Animation
		weapon.GetComponent.<Animation>().Play("PunchRight");	
	
		var hit : RaycastHit;
	
		if(Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), hit)){
			distance = hit.distance;
			
			if(distance < maxDistance){
				hit.transform.SendMessage("ApplyDamage", damage, SendMessageOptions.DontRequireReceiver);
			}
		}
	}
	if (weapon.GetComponent.<Animation>().isPlaying == false){
		weapon.GetComponent.<Animation>().CrossFade("IdleWithWeapon");
	}
	if (Input.GetKey(KeyCode.LeftShift)){
		weapon.GetComponent.<Animation>().CrossFade("Run");
	}
	if (Input.GetKeyUp(KeyCode.LeftShift)){
		weapon.GetComponent.<Animation>().CrossFade("IdleWithWeapon");
	}
}