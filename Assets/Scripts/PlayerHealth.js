﻿#pragma strict

var health : float = 100;
var maxHealth : float = 100;
var healthBar : Slider;
var hitSound : AudioClip;

function Update(){
	if (health <= 0){
		Dead();
	}
	healthBar.value = health/100;
}
function ApplyDamage(damage : float){
	health -= damage;
	AudioSource.PlayClipAtPoint(hitSound, transform.position);
}

function Dead(){
	transform.position = Vector3(136.3519, 0.77, 91.32025);
	health = 100;
}

function addHealth(regen : float){
	// checks to make sure the health does not overflow
	var temp : int;
	temp = health + regen;
	
	if( temp > maxHealth){
		health = 100;
	} else {
		health += regen;
	}
}


